
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class Connectdb {
    public static void main(String[] args) {
        Connection con=null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:"+ dbName);
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open db");
            System.exit(0);
        }
        System.out.println("Opened Successfully");
    }
}
