import java.sql.*;
public class InsertUser {
       public static void main(String[] args) {
        Connection c = null;
        Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         c = DriverManager.getConnection("jdbc:sqlite:user.db");
         System.out.println("Opened database successfully");
         c.setAutoCommit(false);
         stmt = c.createStatement();
         String sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (4, 'Ninth','password' );"; 
         stmt.executeUpdate(sql);
         sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (5, 'Best','password' );"; 
         stmt.executeUpdate(sql);
         stmt.close();
         c.commit();
         c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Table created successfully");
   }
}
